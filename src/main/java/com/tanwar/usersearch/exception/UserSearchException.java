package com.tanwar.usersearch.exception;

import java.io.Serializable;

public class UserSearchException extends RuntimeException implements Serializable {

    private static final long serialVersionUID = -8286890808424815053L;

    public UserSearchException(Throwable cause) {
        super(cause);
    }
}
