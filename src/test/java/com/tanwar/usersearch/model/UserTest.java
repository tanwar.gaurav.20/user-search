package com.tanwar.usersearch.model;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

public class UserTest {

  @Test
  public void testConstructor() {
    User user = new User(
        1,
        "John",
        "Snow",
        "john.snow.got@gmail.com",
        "0.0.0.0",
        123.456,
        654.321);

    assertThat("id has been set", user.getId(), is(1));
    assertThat("firstName has been set", user.getFirstName(), is("John"));
    assertThat("lastName has been set", user.getLastName(), is("Snow"));
    assertThat("email has been set", user.getEmail(), is("john.snow.got@gmail.com"));
    assertThat("ipAddress has been set", user.getIpAddress(), is("0.0.0.0"));
    assertThat("latitude has been set", user.getLatitude(), is(123.456));
    assertThat("longitude has been set", user.getLongitude(), is(654.321));
  }
}