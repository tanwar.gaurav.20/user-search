package com.tanwar.usersearch.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.ResourceAccessException;

/**
 * Exception handler for REST client exceptions
 */
@ControllerAdvice
public class UserSearchExceptionHandler {
    @ExceptionHandler(value = UserSearchException.class)
    public ResponseEntity<Object> exception(UserSearchException exception) {
        return new ResponseEntity<>("Service not available at the moment. Please try again later"
                , HttpStatus.valueOf(503));
    }
}