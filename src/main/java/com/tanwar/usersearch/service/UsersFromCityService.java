package com.tanwar.usersearch.service;

import com.tanwar.usersearch.configuration.CacheConfig;
import com.tanwar.usersearch.exception.UserSearchException;
import com.tanwar.usersearch.model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Service to return all users from a city.
 */
@Service
public class UsersFromCityService {

    /**
     * Base url for backend call.
     */
    private final String backendUrl;

    /**
     * Logger for this service.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UsersWithinRadiusService.class);

    /**
     * Define restTemplate.
     */
    private RestTemplate restTemplate;

    /**
     * Constructor for the Service with restTemplate bean.
     *
     * @param restTemplate used to make the backend request.
     */
    @Autowired
    public UsersFromCityService(RestTemplate restTemplate, final @Value("${backend_service_url}") String url) {
        this.restTemplate = restTemplate;
        this.backendUrl = url;
    }

    /**
     * Get all users in a list from a given city.
     *
     * @param city - String name of city to allow the methid to be reused for future requirements.
     * @return - List of users from given city.
     */
    @Cacheable(CacheConfig.CACHE_CITY_SERVICE)
    public List<User> getUsersFromCity(final String city) {
        final String requestUrl = backendUrl + "city/" + city + "/users";
        ResponseEntity<User[]> responseEntity = null;

        LOGGER.info("Request all users listed as from London.");
        try {
            responseEntity =
                    restTemplate.exchange(requestUrl, HttpMethod.GET, null, User[].class);
        } catch (HttpStatusCodeException e) {
            LOGGER.error("Request to get all users listed from London failed. Error: {}", e.getMessage());
            throw new UserSearchException(e);
        } catch (RestClientException e) {
            LOGGER.error("Request to get all users listed from London failed. Error: {}", e.getMessage());
            throw new UserSearchException(e);
        }
        final User[] users = responseEntity.getBody();

        return users != null ? Arrays.asList(users) : new ArrayList<>();
    }
}