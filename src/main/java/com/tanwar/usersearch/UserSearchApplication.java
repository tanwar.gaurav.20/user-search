package com.tanwar.usersearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.tanwar.usersearch")
public class UserSearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserSearchApplication.class, args);
    }

}
